# 386BOOT
386BOOT is the bootloader intended for use in my OS project DOS\i.
It is already usable, but will not do anything except laoding an ELF file named `KERNEL` to its entry address. Feel free to use it in your hobby project.

## Build it
- Get `yasm` from [somewhere](http://yasm.tortall.net)
- `cd` into the 386BOOT project folder
- type `make`
