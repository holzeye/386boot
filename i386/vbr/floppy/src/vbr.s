;
;  vbr.s
;  nagra
;
;  Created by Alexander Immel on 02.04.14.
;  Copyright (c) 2014 holzeye. All rights reserved.
;

org 0x7C00
bits 16

start:
	jmp main ; Skip all the data

; Force writing BPB to +0x3
times 0x3 - ($ - $$) db 0

; BIOS Parameter Block
%include "bpb.s"

; Logging functions
%include "log.s"

; Floppy Disk Access functions
%include "fda.s"

; FAT12 driver
%include "fat12.s"

; Strings
msg_load db "Loading OSL", NUL
osl.name db "OSL        "

; Bootloader Entry Point
main:
	cli				; CLear Interrupt flag
	xor ax, ax
	mov ds, ax
	mov es, ax

	mov ss, ax
	mov sp, 0x7C00
	sti				; SeT Interrupt flag

	mov si, BPB+OSName
	mov cx, 6
	call putns16

	call fat12.init

	mov si, msg_load
	call puts16

	mov si, osl.name
	push OSL_SEGMENT
	pop es
	xor bx, bx
	call fat12.load

	push word OSL_SEGMENT
	push word 0
	retf

fail:
	cli
	hlt

; Place the magic number at +0x1FE
times 0x1FE - ($ - $$) db 0

; The magic number BIOS uses for verification
dw 0xAA55