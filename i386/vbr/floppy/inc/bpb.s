;
;  bpb.s
;  nagra
;
;  Created by Alexander Immel on 03.04.14.
;  Copyright (c) 2014 holzeye. All rights reserved.
;

%ifndef __nagra__bpb__
%define __nagra__bpb__

bits 16

struc bpb
	OSName:					resb 8
	bytesPerSector:			resw 1
	sectorsPerCluster:		resb 1
	reservedSectorsCount:	resw 1
	FATCount:				resb 1
	rootDirEntryCount:		resw 1
	sectorsCount:			resw 1
	mediaType:				resb 1
	sectorsPerFAT:			resw 1
	sectorsPerCylinder:		resw 1
	headsPerCylinder:		resw 1
	hiddenSectorsCount:		resd 1
	bigSectorsCount:		resd 1
endstruc

struc epb
	driveNumber:			resb 1
	reserved:				resb 1
	extendedSignature:		resb 1
	volumeSerialNumber:		resd 1
	volumeLabel:			resb 11
	fileSystemType:			resb 8
endstruc

BPB:
	istruc bpb
		at OSName,					db "NAGRA   "
		at bytesPerSector,			dw 512
		at sectorsPerCluster,		db 1
		at reservedSectorsCount,	dw 1
		at FATCount,				db 2
		at rootDirEntryCount,		dw 224
		at sectorsCount,			dw 2880
		at mediaType,				db 0xF0
		at sectorsPerFAT,			dw 9
		at sectorsPerCylinder,		dw 18
		at headsPerCylinder,		dw 2
		at hiddenSectorsCount,		dd 0
		at bigSectorsCount,			dd 0
	iend

EPB:
	istruc epb
		at driveNumber,				db 0
		at reserved,				db 0
		at extendedSignature,		db 0x29
		at volumeSerialNumber,		dd 0x4C534F4E
		at volumeLabel,				db "NAGRA      "
		at fileSystemType,			db "FAT12   "
	iend

%endif ;__nagra__bpb__