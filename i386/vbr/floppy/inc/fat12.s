;
;  fat12.s
;  nagra
;
;  Created by Alexander Immel on 04.04.14.
;  Copyright (c) 2014 holzeye. All rights reserved.
;

%ifndef __nagra__fat12__
%define __nagra__fat12__

%include "commons.s"
%include "fda.s"

bits 16

; Constants

; Variables
fat.offset dw 1
fat.length dw 18

rootDir.offset dw 19
rootDir.length dw 14

fileLoadAddress dw 0

; fat12.init - initialize the FAT12 driver
fat12.init:
	pusha

	; Offset of FAT (sectors)
;	mov ax, word [BPB+reservedSectorsCount]
;	mov word [fat.offset], ax
;
;	; Length of FAT (sectors)
;	xor ax, ax
;	mov al, byte [BPB+FATCount]
;	mul word [BPB+sectorsPerFAT]
;	mov word [fat.length], ax
;
;	; Offset of root dir (sectors)
;	mov ax, word [fat.offset]
;	add ax, word [fat.length]
;	mov word [rootDir.offset], ax
;
;	; Length of root dir (sectors)
;	mov ax, 32
;	mul word [BPB+rootDirEntryCount]
;	div word [BPB+bytesPerSector]
;	mov word [rootDir.length], ax

	; Read all the things!
	mov ax, word [fat.offset]
	mov cx, word [fat.length]
	push word DATA_FAT_SEGMENT
	pop es
	xor bx, bx
	xor dx, dx
	call read16

	mov ax, word [rootDir.offset]
	mov cx, word [rootDir.length]
	push word DATA_ROOT_SEGMENT
	pop es
	xor bx, bx
	xor dx, dx
	call read16

	popa
	ret

; fat12.search - look up a file name in the root directory
; < ds:si -- filename to look for
; > ax -- address of first cluster
fat12.search:
	pusha
	mov cx, word [BPB+rootDirEntryCount]
	push DATA_ROOT_SEGMENT
	pop es
	xor di, di
	cld

	.loop
		push cx
		push si
		mov cx, 0xB
		push di
		rep cmpsb	; REPeat CoMPute String Byte
		pop di
		pop si
		pop cx
		je .match

		add di, 32
		loop .loop
		jmp fail
	.match
		mov ax, word [es:di + 0x1A]	; Cluster is at offset 0x1A into entry
		mov word [fileLoadAddress], ax
		popa
		ret

; fat12.search
; < ds:si -- filename to look for
; < es:bx -- load buffer
fat12.load:
	.search
		push es
		push bx
		call fat12.search
	.load
		pop bx
		pop es
		mov ax, word [fileLoadAddress]
		call conv.chscluster_lba
		;mov word [fileLoadAddress], ax
		;mov si, fileLoadAddress
		;mov cx, 2
		;call putns16
		;cli
		;hlt
		xor cx, cx
		mov cl, byte [BPB+sectorsPerCluster]
		mov dx, 1
		call read16
		push es
		push bx

		; calculate next cluster
		push word DATA_SEGMENT
		pop es
		mov bx, DATA_FAT_OFFSET

		mov ax, word [fileLoadAddress]	; get current cluster
		mov cx, ax			; copy current cluster
		mov dx, ax
		shr dx, 0x0001		; divide by two
		add cx, dx			; sum for (3/2)
		add	bx, cx

		mov	dx, word [es:bx]
		test ax, 0x0001	; test for odd or even cluster
		jnz	.odd

	.even
		and	dx, 0x0FFF		; take low 12 bits
		jmp	.done

	.odd
		shr	dx, 4			; take high 12 bits

	.done
		mov	word [fileLoadAddress], dx
		cmp	dx, 0x0FF6		; test for end of file marker
		jb	.load
		pop	bx
		pop es
		ret

%endif ;__nagra__fat12__