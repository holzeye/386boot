;
;  fda16.s
;  nagra
;
;  Created by Alexander Immel on 04.04.14.
;  Copyright (c) 2014 holzeye. All rights reserved.
;

%ifndef __nagra__fda16__
%define __nagra__fda16__

; LBA<->CHS Conversion functions
%include "conv.s"
%include "log.s"

bits 16

; Constants
%define $reset 0
%define $read 0x02

msg_progress db ".", NUL
msg_endl db CR, LF, NUL

; read16 -- read sectors from floppy disk 0
; < ax -- first sector
; < cx -- sector count
; < dx -- bool: show dots?
; < es:bx -- loading buffer address
read16:
	.main
		mov di, 5			; fail_counter

	.loop
		push ax
		push bx
		push cx
		push dx

		call conv.lba_chs	; Get the needed values into ch, cl and dh
		mov ah, $read
		mov al, 1			; Read 1 sector at a time
		mov ch, byte [chs.cylinder]
		mov cl, byte [chs.sector]
		mov dh, byte [chs.head]
		mov dl, [EPB+driveNumber]
		int 0x13
		jnc .done			; Carry flag indicates error

		xor ax, ax			; $reset
		int 0x13			; Reset floppy drive

		pop dx
		pop cx
		pop bx
		pop ax

		dec di				; fail_counter--
		jnz .loop			; if fail_counter > 0
		
		int 0x18			; This machine sucks. Cold reboot.

	.done
		pop dx
		pop cx
		pop bx
		pop ax

		cmp dx, 0
		je .c
		mov si, msg_progress
		call puts16
		.c
		add bx, word [BPB+bytesPerSector]	; Increase load address
		inc ax				; Next sector
		loop .main
		
		ret
		
%endif ;__nagra__fda16__