;
;  osl.s
;  nagra
;
;  Created by Alexander Immel on 06.04.14.
;  Copyright (c) 2014 holzeye. All rights reserved.
;

org 0x0600
bits 16

start:
	jmp main

%include "bpb.s"
%include "log.s"
%include "gdt.s"
%include "a20.s"
%include "fat12.s"
%include "memory.s"

msg_hello db CR, LF, "NAGRA Operating System Loader v0.0.1", CR, LF, NUL
msg_activatepm db "Activating Protected Mode...", CR, LF, NUL
msg_loadcor db "Loading Operating System Kernel", NUL
msg_fail_loadcor db CR, LF, "Failed loading OS. Press any key to reboot.", CR, LF, NUL

cor.name db "KERNEL     "
cor.size dw 0

main:
	cli
	xor ax, ax
	mov ds, ax
	mov es, ax

	mov ax, 0x9000
	mov ss, ax
	mov sp, 0xFFFF
	sti

	mov si, msg_hello
	call puts16

	; Detecting memory. Please wait...

	

	; Now load the kernel into memory...
	
	mov si, msg_loadcor
	call puts16

	call fat12.init

	; TODO: check EBDA address!!!

	mov si, cor.name
	push word COR_RMODE_SEGMENT
	pop es
	xor bx, bx
	call fat12.load

	; ax contains size in clusters (returned by load())
	mul word [BPB+sectorsPerCluster]
	mul word [BPB+bytesPerSector]
	mov word [cor.size], ax

	mov si, msg_activatepm
	call puts16

	call a20.activate
	call gdt.init

	cli
	mov eax, cr0
	or eax, 1
	mov cr0, eax

	jmp 0x08:protectedmode

fail:
	mov si, msg_fail_loadcor
	call puts16
	xor ax, ax
	int 0x16
	int 0x19


;---------------------------[ BEGIN OF 32-BIT CODE ]----------------------------

bits 32

%include "log32.s"

msg_pmactive db "Protected Mode active.", CR, LF, NUL
msg_movecor db "Moving kernel to 1MiB...", CR, LF, NUL

protectedmode:
	cli
	mov ax, 0x10
	mov ds, ax
	mov es, ax
	mov ss, ax
	mov esp, 0x90000

	call clear

	mov esi, msg_pmactive
	call puts

	mov esi, msg_movecor
	call puts

	movzx ecx, word [cor.size]
	mov esi, COR_RMODE_ADDRESS
	mov edi, COR_PMODE_ADDRESS
	cld
	rep movsb ; Maybe use movsd?

	jmp 0x8:COR_PMODE_ADDRESS

	cli
	hlt

times 0x600 - ($ - $$) db 0