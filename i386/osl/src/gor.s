;
;  gor.s
;  nagra
;
;  Created by Alexander Immel on 12.04.14.
;  Copyright (c) 2014 holzeye. All rights reserved.
;

org 0x100000
bits 32

start:
	jmp main

%include "log32.s"

msg_hello db "Hello Kernel!", CR, LF, NUL

main:
	cli
	mov ax, 0x10
	mov ds, ax
	mov es, ax
	mov ss, ax
	mov esp, 0x90000

	call clear
	mov esi, msg_hello
	call puts
	mov esi, msg_hello
	call puts

	cli
	hlt

times 0x200 - ($ - $$) db 0