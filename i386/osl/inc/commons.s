;
;  commons.s
;  nagra
;
;  Created by Alexander Immel on 04.04.14.
;  Copyright (c) 2014 holzeye. All rights reserved.
;

%ifndef __nagra__commons__
%define __nagra__commons__

bits 16

%define COR_RMODE_ADDRESS 0xBA00
%define COR_RMODE_SEGMENT 0x0BA0
%define COR_PMODE_ADDRESS 0x100000

%define MULTIBOOT_STRUCTURE_SEGMENT 0x0050
%define MULTIBOOT_STRUCTURE_ADDRESS 0x0500

%define MULTIBOOT_DATA_SEGMENT 0x07C0
%define MULTIBOOT_DATA_ADDRESS 0x7C00

%define DATA_ADDRESS 0x7E00
%define DATA_SEGMENT 0x07E0
%define DATA_FAT_ADDRESS 0x7E00
%define DATA_FAT_SEGMENT 0x07E0
%define DATA_FAT_OFFSET 0x0000
%define DATA_ROOT_ADDRESS 0x9E00
%define DATA_ROOT_SEGMENT 0x09E0
%define DATA_ROOT_OFFSET 0x2000
%define DATA_END_ADDRESS 0xBA00

%endif ;__nagra__commons__