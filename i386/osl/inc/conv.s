;
;  conv.s
;  nagra
;
;  Created by Alexander Immel on 04.04.14.
;  Copyright (c) 2014 holzeye. All rights reserved.
;

%ifndef __nagra__conv__
%define __nagra__conv__

bits 16

chs.sector db 0
chs.head db 0
chs.cylinder db 0

; conv.lba_chs - convert an LBA address to CHS
; < ax -- LBA address to convert
; > chs.cylinder
; > chs.sector
; > chs.head
conv.lba_chs
	xor dx, dx
	div word [BPB+sectorsPerCylinder]
	inc dl 							; sectors counted from 1
	mov byte [chs.sector], dl 		; CHS sector
	xor dx, dx
	div word [BPB+headsPerCylinder]
	mov byte [chs.head], dl 		; CHS head
	mov byte [chs.cylinder], al 	; CHS cylinder
	ret

; conv.chs_lba - convert a CHS dataset to LBA
; < chs.cylinder
; < chs.sector
; < chs.head
; > ax -- LBA address
;conv.chs_lba:
;	mov al, byte [chs.cylinder]
;	mul word [BPB+headsPerCylinder]
;	add al, byte [chs.head]
;	mul word [BPB+sectorsPerCylinder]
;	push bx
;	mov bx, ax
;	xor ax, ax
;	mov al, byte [chs.sector]
;	sub ax, 1
;	add ax, bx
;	pop bx
;	ret

; conv.chscluster_lba - convert a CHS cluster to LBA
; < ax -- CHS cluster
; > ax -- LBA address
conv.chscluster_lba:
	sub ax, 2
	mul byte [BPB+sectorsPerCluster]
	add al, byte [rootDir.offset]
	add al, byte [rootDir.length]
	ret

; conv.lba_chscluster - convert an LBA address to a CHS cluster
; < ax -- LBA address
; > ax -- CHS cluster
;conv.lba_chscluster:
;	sub al, byte [rootDir.offset]
;	sub al, byte [rootDir.length]
;	div word [BPB+sectorsPerCluster]
;	add ax, 2

%endif ;__nagra__conv__as