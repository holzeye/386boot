;
;  a20.s
;  nagra
;
;  Created by Alexander Immel on 07.04.14.
;  Copyright (c) 2014 holzeye. All rights reserved.
;

%ifndef __nagra__a20__
%define __nagra__a20__

bits 16

wait_input:
	in al, 0x64
	test al, 2
	jnz wait_input
	ret

wait_output:
	in al, 0x64
	test al, 1
	jz wait_output
	ret

a20.activate:
	cli
	pusha

	call wait_input
	mov al, 0xAD
	out 0x64, al
	call wait_input

	mov al, 0xD0
	out 0x64, al
	call wait_output

	in al, 0x60
	push eax
	call wait_input

	mov al, 0xD1
	out 0x64, al
	call wait_input

	pop eax
	or al, 2
	out 0x60, al

	call wait_input
	mov al, 0xAE
	out 0x64, al
	call wait_input

	popa
	sti
	ret

%endif ;__nagra__a20__