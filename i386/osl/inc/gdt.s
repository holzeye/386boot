;
;  gdt.s
;  nagra
;
;  Created by Alexander Immel on 07.04.14.
;  Copyright (c) 2014 holzeye. All rights reserved.
;

%ifndef __nagra__gdt__
%define __nagra__gdt__

bits 16

gdt.init:
	pusha
	cli
	lgdt [gdtp]
	sti
	popa
	ret

gdt:
gdt_null:
	dd 0
	dd 0
gdt_code:
	dw 0xFFFF
	dw 0
	db 0
	db 10011010b
	db 11001111b
	db 0
gdt_data:
	dw 0xFFFF
	dw 0
	db 0
	db 10010010b
	db 11001111b
	db 0
gdt_end:

gdtp:
	dw gdt_end - gdt - 1
	dd gdt

%endif ;__nagra__gdt__