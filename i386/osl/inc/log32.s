;
;  log32.s
;  nagra
;
;  Created by Alexander Immel on 08.04.14.
;  Copyright (c) 2014 holzeye. All rights reserved.
;

%ifndef __nagra__log32__
%define __nagra__log32__

bits 32

%define LF 0x0A
%define CR 0x0D
%define NUL 0x0

%define VMEM_ADDRESS 0xB8000
%define BYTES_PER_CHAR 2
%define CR_X_MAX 80
%define CR_Y_MAX 25

%define LF 0x0A
%define CR 0x0D
%define NUL 0x0

cr.x db 0
cr.y db 0
color db 0x07

; putc - print a character to the screen
; < bl -- character to print
putc:
	pusha
	mov edi, VMEM_ADDRESS

	xor eax, eax
	mov ecx, CR_X_MAX * BYTES_PER_CHAR
	mov al, byte [cr.y]
	mul ecx
	push eax
	mov al, byte [cr.x]
	mov ecx, BYTES_PER_CHAR
	mul ecx
	pop ecx
	add eax, ecx
	add edi, eax

	cmp bl, NUL
	je .done
	cmp bl, CR
	je .cr
	cmp bl, LF
	je .lf

	mov bh, byte [color]
	mov word [edi], bx

	inc byte [cr.x]
	cmp byte [cr.x], CR_X_MAX
	jne .done

	.lf
		mov byte [cr.x], 0
		inc byte [cr.y]
		jmp .done

	.cr
		mov byte [cr.x], 0
		jmp .done

	.done
		popa
		ret

; puts - print a string to the screen
; < esi -- address of (null-terminated) string
puts:
	pusha
	.loop
		mov bl, byte [esi]
		cmp bl, NUL
		je .done
		call putc
		inc esi
		jmp .loop

	.done
		mov bh, byte [cr.y]
		mov bl, byte [cr.x]
		call mvcr
		
		popa
		ret

; stcl - set the print color
; < bh -- background
; < bl -- foreground
stcl:
	shl bh, 4
	or bl, bh
	mov byte [color], bl
	ret

; mvcr - move the cursor
; < bh -- y value
; < bl -- x value
mvcr:
	pusha

	xor ax, ax
	mov al, bh
	mov ecx, CR_Y_MAX
	mul ecx
	add al, bl
	mov bx, ax

	mov dx, 0x304
	mov al, 0xE
	out dx, al

	mov dx, 0x305
	mov al, bh
	out dx, al

	mov dx, 0x304
	mov al, 0xF
	out dx, al

	mov dx, 0x305
	mov al, bl
	out dx, al

	popa
	ret

; clear - clear the screen
clear:
	pusha

	mov edi, VMEM_ADDRESS
	xor bx, bx
	mov ecx, CR_X_MAX * CR_Y_MAX * BYTES_PER_CHAR
	rep stosw

	mov byte [cr.x], 0
	mov byte [cr.y], 0

	popa
	ret

%endif ;__nagra__log32__