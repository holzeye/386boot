;
;  log16.s
;  nagra
;
;  Created by Alexander Immel on 03.04.14.
;  Copyright (c) 2014 holzeye. All rights reserved.
;

%ifndef __nagra__log16__
%define __nagra__log16__

bits 16

%define LF 0x0A
%define CR 0x0D
%define NUL 0x0

; puts16 - print a string to the screen
; < ds:si -- pointer to string
puts16:
	push ax
	push bx
	mov ah, 0x0E
	mov bx, 0x0007
	.loop
		lodsb
		or al, al
		jz .done
		int 0x10
		jmp .loop
	.done
		pop bx
		pop ax
		ret

; putns16 - print n bytes of a string to the screen
; < cx -- character count
; < ds:si -- pointer to string
putns16:
	push ax
	push bx
	push cx
	mov ah, 0x0E
	mov bx, 0x0007
	.loop
		lodsb
;		or al, al
;		jz .done
		cmp al, 0x20
		je .sp
		cmp al, 0xFF
		je .sp
		.r
		int 0x10
		loop .loop
	.done
		pop cx
		pop bx
		pop ax
		ret
	.sp
		mov al, 0x7F
		jmp .r

%endif ;__nagra__log16__