;
;  memory.s
;  nagra
;
;  Created by Alexander Immel on 18.04.14.
;  Copyright (c) 2014 holzeye. All rights reserved.
;

%ifndef __nagra__memory__
%define __nagra__memory__

bits 16

%define SMAP 0x50414D53

; Memory Types
; 0: Free/Usable Memory
; 1: Reserved Memory
; 2: ACPI Reclaimable Memory - usable after reading ACPI tables
; 3: ACPI NVS Memory - must be saved after NVS(?) session

struc memoryMapEntry
	.baseAddress resq 1
	.length		resq 1
	.type		resd 1
	.reserved	resd 1
endstruc

; getMemorySize - returns memory size below 64KiB. works on EVERY system.
; > ax -- memory size or -1 for error
getMemorySize:
	int 0x12
	jc .error

	test ax, ax
	jz .error
	cmp ah, 0x80 ; Invalid command?
	je .error
	cmp ah, 0x86 ; Unsupported function?
	je .error

	ret
	.error
		mov ax, -1
		ret

; getExtendedMemorySize - returns memory size starting at 1MiB
; > ax -- memory size or -1 for error
getExtendedMemorySize:
	mov ax, 0x88
	int 0x15
	jc .error

	test ax, ax
	jz .error
	cmp ah, 0x80 ; Invalid command?
	je .error
	cmp ah, 0x86 ; Unsupported function?
	je .error

	ret
	.error:
		mov ax, -1
		ret

; getBigMemorySize - returns memory size for systems with more than 64MiB RAM
; > ax -- memory between 1MiB and 16MiB in KiB
; > bx -- memory above 16MiB in 64KiB blocks
getBigMemorySize:
	push ecx
	push edx

	mov eax, 0xE801
	int 0x15
	jc .error

	cmp ah, 0x80 ; Invalid command?
	je .error
	cmp ah, 0x86 ; Unsupported function?
	je .error

	jcxz .axbx	; Jump if CX is Zero
	mov ax, cx
	mov bx, dx
	pop edx
	pop ecx
	ret

	.axbx
		pop edx
		pop ecx
		ret

	.error
		mov ax, -1
		mov bx, -1
		pop edx
		pop ecx
		ret

; getMemoryMap - returns a nice map of the memory
; < es:di -- pointer to memory map
; > bp -- number of entries
getMemoryMap:
	pushad

	mov eax, 0xE820
	xor ebx, ebx
	mov ecx, 24
	mov edx, SMAP
	int 0x15
	jc .error

	cmp eax, SMAP
	jne .error

	test ebx, ebx
	je .error

	jmp .start

	.next
		mov eax, 0xE820
		mov ecx, 24
		mov edx, SMAP
		int 0x15

	.start
		jcxz .skip

	.verify
		mov ecx, dword [es:di + memoryMapEntry.length]
		test ecx, ecx
		jne short .good
		mov ecx, dword [es:di + memoryMapEntry.length + 4]
		jcxz .skip

	.good
		inc bp
		add di, 24

	.skip
		cmp ebx, 0
		jne .next
		jmp .done

	.error
		stc

	.done
		popad
		ret


%endif ;__nagra__memory__