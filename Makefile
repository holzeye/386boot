#===============================================================================
# NAGRA BOOTLOADER
# COPYRIGHT (C) 2013-2014 ALEXANDER IMMEL
# ALL RIGHTS RESERVED.
#
# /MAKEFILE - THE MASTER MAKEFILE
#===============================================================================

include ./CONFIG

#===============================================================================
# CONSTANTS

TGT_MAIN	   	:= "NAGRA Bootloader"
TGT_VBR			:= "NAGRA VBR"
TGT_OSL			:= "NAGRA OS Loader"
TGT_DIST		:= "Floppy Disk"
#TGT_TOOLS		:= "Tools"

#===============================================================================
# MAKE TARGETS

all: vbr osl dist

vbr:
	@echo $(DIR)$(W_BUILD) $(TGT_VBR)
	@make -C $(DIR_VBR)

osl:
	@echo $(DIR)$(W_BUILD) $(TGT_OSL)
	@make -C $(DIR_OSL)

dist:
	@echo $(DIR)$(W_CREATE) $(TGT_DIST)
	@make -C $(DIR_DIST)

#tools:
#	@echo $(DIR)$(W_COMPILE) $(TGT_TOOLS)
#	@make -C $(DIR_TOOLS)

test: all
	@$(QEMU) $(FLAGS_QEMU) -fda $(DIR_DIST)floppy.img

test-only:
	@- $(QEMU) $(FLAGS_QEMU) -fda $(DIR_DIST)floppy.img

clean:
	@echo $(DIR)$(W_CLEAN) $(TGT_MAIN)
	@make -C $(DIR_VBR)		clean
	@make -C $(DIR_OSL)		clean
#	@make -C $(DIR_TOOLS)	clean
	@make -C $(DIR_DIST)	clean

xclean:
	@echo $(DIR)$(W_CLEAN) $(TGT_MAIN)
	@make -C $(DIR_VBR)		xclean
	@make -C $(DIR_OSL)		xclean
#	@make -C $(DIR_TOOLS)	xclean
	@make -C $(DIR_DIST)	xclean

#===============================================================================
# PHONY TARGETS

.PHONY: all vbr osl dist test test-only clean xclean #tools

#===============================================================================
# /MAKEFILE END
#===============================================================================
